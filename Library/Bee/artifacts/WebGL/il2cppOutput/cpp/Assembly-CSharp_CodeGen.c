﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Automate::Start()
extern void Automate_Start_mBDB0AF6E210594E747FCF3F89B7603AD0E3CD830 (void);
// 0x00000002 System.Void Automate::DoMove(System.String)
extern void Automate_DoMove_m4F15202D7B3A33C0188CF9BB02DE1B6B7F08D4F1 (void);
// 0x00000003 System.Void Automate::Update()
extern void Automate_Update_m34C1E56FF6EF2E6650A5E44296CEA916C8009C87 (void);
// 0x00000004 System.Void Automate::RotateSide(System.Collections.Generic.List`1<UnityEngine.GameObject>,System.Single)
extern void Automate_RotateSide_m2A90885AE972850C24FA51C5DBFA63CC65008D99 (void);
// 0x00000005 System.Void Automate::Shuffle()
extern void Automate_Shuffle_m2FC45E731D05A8321449F78B3E04812B61F13221 (void);
// 0x00000006 System.Void Automate::.ctor()
extern void Automate__ctor_m5D87D2488028FEB05629C5FA1C12DA1F4D00DF3A (void);
// 0x00000007 System.Void Automate::.cctor()
extern void Automate__cctor_mEB1D86BAC29210F348DB2C7DF6DF1FC189DCBBBF (void);
// 0x00000008 System.Void CubeMap::Start()
extern void CubeMap_Start_mC6C563E05776B0A0620CD814B3677471A400AB2B (void);
// 0x00000009 System.Void CubeMap::Update()
extern void CubeMap_Update_mE4F60140B1651C3C44DACF3AA5AB616BE4271B9C (void);
// 0x0000000A System.Void CubeMap::Set()
extern void CubeMap_Set_m615B714D1D5802CA5611CFF04326F78BDDBA8DF8 (void);
// 0x0000000B System.Void CubeMap::UpdateMap(System.Collections.Generic.List`1<UnityEngine.GameObject>,UnityEngine.Transform)
extern void CubeMap_UpdateMap_mCC226C86B1F042627D8601B54047E3BC5053A107 (void);
// 0x0000000C System.Void CubeMap::.ctor()
extern void CubeMap__ctor_m4817E9BA5EAB2AA13CB0813EA9165A52FBC363D5 (void);
// 0x0000000D System.Void CubeMap2x2::Start()
extern void CubeMap2x2_Start_mD254DE5DE9661A8BE9E594082FDB25D98651B30E (void);
// 0x0000000E System.Void CubeMap2x2::Update()
extern void CubeMap2x2_Update_m8FFD84CA1CE28D1D397EF34BE481D16FA79785B6 (void);
// 0x0000000F System.Void CubeMap2x2::Set()
extern void CubeMap2x2_Set_m270BBB103090C4316BF67E7B1CBF9DF3689FCBD5 (void);
// 0x00000010 System.Void CubeMap2x2::UpdateMap(System.Collections.Generic.List`1<UnityEngine.GameObject>,UnityEngine.Transform)
extern void CubeMap2x2_UpdateMap_m089088BDDA6590467C40C1CB00A1D6ED6DA433A4 (void);
// 0x00000011 System.Void CubeMap2x2::.ctor()
extern void CubeMap2x2__ctor_mBDC2C176325512FADD5E8E8FD243F7B0A525E6CD (void);
// 0x00000012 System.Void CubeState::Start()
extern void CubeState_Start_mEA2AA4C1915D4066A2832B6B09E0212D8FD2769F (void);
// 0x00000013 System.Void CubeState::Update()
extern void CubeState_Update_m8576056AFD6EFAD1AC156B622F05FA5E7899992E (void);
// 0x00000014 System.Void CubeState::PickUp(System.Collections.Generic.List`1<UnityEngine.GameObject>)
extern void CubeState_PickUp_m87F4D7675578FD605B8734B954700BE931777E8D (void);
// 0x00000015 System.Void CubeState::PutDown(System.Collections.Generic.List`1<UnityEngine.GameObject>,UnityEngine.Transform)
extern void CubeState_PutDown_mC4920A3F8E8E994A725C711194BC2837489F4E63 (void);
// 0x00000016 System.String CubeState::GetSideString(System.Collections.Generic.List`1<UnityEngine.GameObject>)
extern void CubeState_GetSideString_mAA408019C3B3E72E5FC67F83F58D9F63BE3777F0 (void);
// 0x00000017 System.String CubeState::GetStateString()
extern void CubeState_GetStateString_m76ED7BF10189A348F41AF0ECF0C91EED5E845FAD (void);
// 0x00000018 System.Void CubeState::.ctor()
extern void CubeState__ctor_m9AC614D3D99268EB42439D4B44DF0F6993571A66 (void);
// 0x00000019 System.Void PivotRotation::Start()
extern void PivotRotation_Start_m86D01248253987BDC15EB167889A95FFABA80B94 (void);
// 0x0000001A System.Void PivotRotation::LateUpdate()
extern void PivotRotation_LateUpdate_m711A34DA3E5604B83DCE1BB5285CB98BA86BA2E0 (void);
// 0x0000001B System.Void PivotRotation::SpinSide(System.Collections.Generic.List`1<UnityEngine.GameObject>)
extern void PivotRotation_SpinSide_m92B1DE185796F59EF61BB269B8528F89CC114EBA (void);
// 0x0000001C System.Void PivotRotation::Rotate(System.Collections.Generic.List`1<UnityEngine.GameObject>)
extern void PivotRotation_Rotate_mC187B184E0DD7BE4C151331B1CE3361AF66AFE1B (void);
// 0x0000001D System.Void PivotRotation::StartAutoRotate(System.Collections.Generic.List`1<UnityEngine.GameObject>,System.Single)
extern void PivotRotation_StartAutoRotate_m2ADC5074BCF8D24083EF59E70686735F73371B2C (void);
// 0x0000001E System.Void PivotRotation::RotateToRightAngle()
extern void PivotRotation_RotateToRightAngle_mD451F4278A3EA0FD69A7DAF519D281405C12769A (void);
// 0x0000001F System.Void PivotRotation::AutoRotate()
extern void PivotRotation_AutoRotate_m02783AE1AD1051CE09DD092DC59770B505340F34 (void);
// 0x00000020 System.Void PivotRotation::.ctor()
extern void PivotRotation__ctor_mA5504347F2AD2C268AD02F51F990B37D48136B79 (void);
// 0x00000021 System.Void ReadCube::Start()
extern void ReadCube_Start_mCBB1787A677FEFF0F13F849733DEA72C8B1220D9 (void);
// 0x00000022 System.Void ReadCube::Update()
extern void ReadCube_Update_m22D3232C518D5908C9CBFC02DDD1D6FEEA829016 (void);
// 0x00000023 System.Void ReadCube::ReadState()
extern void ReadCube_ReadState_mE1CE672EE789CD1F061A456D5EFEAA86D7214C4F (void);
// 0x00000024 System.Void ReadCube::SetRayTransforms()
extern void ReadCube_SetRayTransforms_m1BF7F2B9C2F28422AC87CCD9F88F3E8DC8005F33 (void);
// 0x00000025 System.Collections.Generic.List`1<UnityEngine.GameObject> ReadCube::BuildRays(UnityEngine.Transform,UnityEngine.Vector3)
extern void ReadCube_BuildRays_m66BCF4EC0D81F94ED9371288422754E676E4D8EB (void);
// 0x00000026 System.Collections.Generic.List`1<UnityEngine.GameObject> ReadCube::ReadFace(System.Collections.Generic.List`1<UnityEngine.GameObject>,UnityEngine.Transform)
extern void ReadCube_ReadFace_m62F0200A60B46E0DE485C988CEEB42EFB0BAAB44 (void);
// 0x00000027 System.Void ReadCube::.ctor()
extern void ReadCube__ctor_m86B33B35EFB6179CF6D880471F5023F2516276BC (void);
// 0x00000028 System.Void ReadCube2x2::Start()
extern void ReadCube2x2_Start_m4629275DE89CDD940D92C2FFB0C882C73AAC9AFE (void);
// 0x00000029 System.Void ReadCube2x2::Update()
extern void ReadCube2x2_Update_m30509ED389B2277C62C8FCF192D00A42F4FCD332 (void);
// 0x0000002A System.Void ReadCube2x2::.ctor()
extern void ReadCube2x2__ctor_mCF0F803238D00B8F922866347F5DA783EC5490AD (void);
// 0x0000002B System.Void RotateBigCube::Start()
extern void RotateBigCube_Start_m11CF76FABEB44B5D94E9428424121B05F27BFA4E (void);
// 0x0000002C System.Void RotateBigCube::Update()
extern void RotateBigCube_Update_m602101730D3E7BF12AD8861B98A454D642F6FFE9 (void);
// 0x0000002D System.Void RotateBigCube::Drag()
extern void RotateBigCube_Drag_m6638ADF4EF83C704461ECBC8F4F090879890039B (void);
// 0x0000002E System.Void RotateBigCube::Swipe()
extern void RotateBigCube_Swipe_m23796E9DCA6E660401B5D1A08639CEF10D01E28B (void);
// 0x0000002F System.Boolean RotateBigCube::LeftSwipe(UnityEngine.Vector2)
extern void RotateBigCube_LeftSwipe_m8BEC0F12B23361056169481F41310842C8AD8CF0 (void);
// 0x00000030 System.Boolean RotateBigCube::RightSwipe(UnityEngine.Vector2)
extern void RotateBigCube_RightSwipe_m27273211A07E0260B1AC83D37717860E17794826 (void);
// 0x00000031 System.Boolean RotateBigCube::UpLeftSwipe(UnityEngine.Vector2)
extern void RotateBigCube_UpLeftSwipe_mA900D93941403FDE22CC9DF050D30A0479E56CBC (void);
// 0x00000032 System.Boolean RotateBigCube::UpRightSwipe(UnityEngine.Vector2)
extern void RotateBigCube_UpRightSwipe_mD76FE6B96A6CD970C073CFEBCBBABE68CA164FE4 (void);
// 0x00000033 System.Boolean RotateBigCube::DownLeftSwipe(UnityEngine.Vector2)
extern void RotateBigCube_DownLeftSwipe_m7DF37C53EFFBD47FCEC6F724D8012B73E24E49E7 (void);
// 0x00000034 System.Boolean RotateBigCube::DownRightSwipe(UnityEngine.Vector2)
extern void RotateBigCube_DownRightSwipe_m7FD9A200C38131A702F6678E47396E0BE44227C2 (void);
// 0x00000035 System.Void RotateBigCube::.ctor()
extern void RotateBigCube__ctor_m775AC20AC064C99B8ECD953DBF231F90889BAAE9 (void);
// 0x00000036 System.Void SelectFace::Start()
extern void SelectFace_Start_m6B8CAF850E5A0DE138D58875FE17FD796682A0AB (void);
// 0x00000037 System.Void SelectFace::Update()
extern void SelectFace_Update_mEA3B8E67A4A36842BB1D70343AE8B10830C78615 (void);
// 0x00000038 System.Void SelectFace::.ctor()
extern void SelectFace__ctor_mE2D218F6E91D0DDBBD0CE9062D15EE5DC4EB7FA6 (void);
// 0x00000039 System.Void SolveTwoPhase::Start()
extern void SolveTwoPhase_Start_m0BDA3CE525AACFEC1DDEE70D8BFFC30C68BF7595 (void);
// 0x0000003A System.Void SolveTwoPhase::Update()
extern void SolveTwoPhase_Update_mF7D483EE9639F9DB8BF78A0E4AF9F66283B29F1A (void);
// 0x0000003B System.Void SolveTwoPhase::Solver()
extern void SolveTwoPhase_Solver_m6ECBAC39E2EE97BE34027BBC08992FD5EB98026C (void);
// 0x0000003C System.Collections.Generic.List`1<System.String> SolveTwoPhase::StringToList(System.String)
extern void SolveTwoPhase_StringToList_m43419C5E86322DEDB05A0689E74FCE995F78CF64 (void);
// 0x0000003D System.Void SolveTwoPhase::.ctor()
extern void SolveTwoPhase__ctor_mC6865BD683042E5DDB12492BF29FC1C70D517789 (void);
// 0x0000003E System.Void UIConfig::Start()
extern void UIConfig_Start_m25B7619856DEB3AEE96DA33C6E3F0848BFE95EF8 (void);
// 0x0000003F System.Void UIConfig::Update()
extern void UIConfig_Update_m3B8D2CF963D09ECD9E9F04F3F85D6FDBD7CFA07B (void);
// 0x00000040 System.Void UIConfig::OpenPanel()
extern void UIConfig_OpenPanel_m018C4AA7A9556A24926B64CC53E75A25FCA5B026 (void);
// 0x00000041 System.Void UIConfig::ClosePanel()
extern void UIConfig_ClosePanel_mAEB1D424F5149A6B34B3EE12112E8C04FE305FEC (void);
// 0x00000042 System.Void UIConfig::.ctor()
extern void UIConfig__ctor_mAE057D107A8E87DBC0CEEEB5E294E97C61B254D0 (void);
// 0x00000043 System.Void Kociemba.CoordCube::.ctor(Kociemba.CubieCube,System.DateTime,System.String,System.String&)
extern void CoordCube__ctor_mFA5B2A4172D7942C1BAEE609FFBD9FABD936D2E8 (void);
// 0x00000044 System.Void Kociemba.CoordCube::move(System.Int32)
extern void CoordCube_move_m8D8DBD684BB5D54613A361246B5EF598DA90B33E (void);
// 0x00000045 System.Void Kociemba.CoordCube::setPruning(System.SByte[],System.Int32,System.SByte)
extern void CoordCube_setPruning_mE9A6455AF77E31D9223B2D7ABE557521C45D01F9 (void);
// 0x00000046 System.SByte Kociemba.CoordCube::getPruning(System.SByte[],System.Int32)
extern void CoordCube_getPruning_mE8CAF02D3C74122D1804E2883BCB3CA5B6D27894 (void);
// 0x00000047 System.Void Kociemba.CoordCube::.cctor()
extern void CoordCube__cctor_mFD2EC6E61CD35995CAEE73EFCD26B3DDC38B5E9E (void);
// 0x00000048 System.Void Kociemba.CoordCubeTables::.cctor()
extern void CoordCubeTables__cctor_m85AACC9F1F41BA4705EF57305973314B07CED64F (void);
// 0x00000049 System.Void Kociemba.CoordCubeBuildTables::.ctor(Kociemba.CubieCube,System.Boolean)
extern void CoordCubeBuildTables__ctor_m8AB3DA09766C3FCA0B88ECB408FD2821B8652E4D (void);
// 0x0000004A System.Void Kociemba.CoordCubeBuildTables::move(System.Int32)
extern void CoordCubeBuildTables_move_mAC7124234589FB1979CE9E2806602C0FBD80984C (void);
// 0x0000004B System.Void Kociemba.CoordCubeBuildTables::.cctor()
extern void CoordCubeBuildTables__cctor_mF2E1E6437BB68FF0171C0D501D966014132E3BC6 (void);
// 0x0000004C System.Void Kociemba.CoordCubeBuildTables::setPruning(System.SByte[],System.Int32,System.SByte)
extern void CoordCubeBuildTables_setPruning_m6FF26F7B216589625A69C06154F7AA71F6B83AEC (void);
// 0x0000004D System.SByte Kociemba.CoordCubeBuildTables::getPruning(System.SByte[],System.Int32)
extern void CoordCubeBuildTables_getPruning_m642B13917BD8B3BD134D276959DA94B5A9DB8CF9 (void);
// 0x0000004E Kociemba.CubieCube Kociemba.CubieCube::SetMoveU()
extern void CubieCube_SetMoveU_m7D4FAE08297C79C16FB107639CE14F649AC50174 (void);
// 0x0000004F Kociemba.CubieCube Kociemba.CubieCube::SetMoveR()
extern void CubieCube_SetMoveR_m6A6140987EEA4B70D770615D59787E2D1499518C (void);
// 0x00000050 Kociemba.CubieCube Kociemba.CubieCube::SetMoveF()
extern void CubieCube_SetMoveF_mF3C7898A58C45515845574F73B5B63286D54CB33 (void);
// 0x00000051 Kociemba.CubieCube Kociemba.CubieCube::SetMoveD()
extern void CubieCube_SetMoveD_m32C25AF0AB36F4B8604E13C310423B7663B41337 (void);
// 0x00000052 Kociemba.CubieCube Kociemba.CubieCube::SetMoveL()
extern void CubieCube_SetMoveL_m3180D3A6CE06A9A515D63C28200107BAA4B70DCB (void);
// 0x00000053 Kociemba.CubieCube Kociemba.CubieCube::SetMoveB()
extern void CubieCube_SetMoveB_m1F735EFBF6457E2095CC087ADD4957F9D04A20DE (void);
// 0x00000054 System.Void Kociemba.CubieCube::.ctor()
extern void CubieCube__ctor_mB2558B39546E22E61E617B78DCB3C1C0C04C21F8 (void);
// 0x00000055 System.Void Kociemba.CubieCube::.ctor(Kociemba.Corner[],System.Byte[],Kociemba.Edge[],System.Byte[])
extern void CubieCube__ctor_mAA57E665AB7511499DF2699079B62DC1E42E3C06 (void);
// 0x00000056 System.Int32 Kociemba.CubieCube::Cnk(System.Int32,System.Int32)
extern void CubieCube_Cnk_m98D7B2B8A75B168B81B3242D387EB8F94A95678D (void);
// 0x00000057 System.Void Kociemba.CubieCube::rotateLeft(Kociemba.Corner[],System.Int32,System.Int32)
extern void CubieCube_rotateLeft_m94DAB9B32516F2E4779EEF3BCC9B31155CA12E8C (void);
// 0x00000058 System.Void Kociemba.CubieCube::rotateRight(Kociemba.Corner[],System.Int32,System.Int32)
extern void CubieCube_rotateRight_mCCF1F161DB9D871DCD9E90E9DA676FACCB5A3302 (void);
// 0x00000059 System.Void Kociemba.CubieCube::rotateLeft(Kociemba.Edge[],System.Int32,System.Int32)
extern void CubieCube_rotateLeft_m4FC7EEA9E12AD2781CF4DA24851B80E88F2BFFFB (void);
// 0x0000005A System.Void Kociemba.CubieCube::rotateRight(Kociemba.Edge[],System.Int32,System.Int32)
extern void CubieCube_rotateRight_mB0355658688C284A59D6C17A1923BFA93E4334E7 (void);
// 0x0000005B Kociemba.FaceCube Kociemba.CubieCube::toFaceCube()
extern void CubieCube_toFaceCube_m69B0E445C8A6BA456898D7C81B0C994AFD8D424C (void);
// 0x0000005C System.Void Kociemba.CubieCube::cornerMultiply(Kociemba.CubieCube)
extern void CubieCube_cornerMultiply_m5331F948D9A0B65AFFE8D2B98301035DB5107191 (void);
// 0x0000005D System.Void Kociemba.CubieCube::edgeMultiply(Kociemba.CubieCube)
extern void CubieCube_edgeMultiply_m0DD6CDD04C35145DA1EE9150FF692BF05015B508 (void);
// 0x0000005E System.Void Kociemba.CubieCube::multiply(Kociemba.CubieCube)
extern void CubieCube_multiply_m16F3D906044236B346C24058D98E21A060F35A69 (void);
// 0x0000005F System.Void Kociemba.CubieCube::invCubieCube(Kociemba.CubieCube)
extern void CubieCube_invCubieCube_m9039266F9DF891EC002A9F14154547B2060AE3D8 (void);
// 0x00000060 System.Int16 Kociemba.CubieCube::getTwist()
extern void CubieCube_getTwist_m7A91EEF586A0B3A87902730155F04B602B47B479 (void);
// 0x00000061 System.Void Kociemba.CubieCube::setTwist(System.Int16)
extern void CubieCube_setTwist_m46C19E28D3BACE0B29EEC5943DE41EDF5A74FB2F (void);
// 0x00000062 System.Int16 Kociemba.CubieCube::getFlip()
extern void CubieCube_getFlip_m680327B28CFB582596E53C606CEA27761289C3BA (void);
// 0x00000063 System.Void Kociemba.CubieCube::setFlip(System.Int16)
extern void CubieCube_setFlip_m5BF099E8040B3EFD04F0A48E7FF23F1AF5F54D86 (void);
// 0x00000064 System.Int16 Kociemba.CubieCube::cornerParity()
extern void CubieCube_cornerParity_mB0E00AE6ED334FB985343E94D45AEF0F21E4DB90 (void);
// 0x00000065 System.Int16 Kociemba.CubieCube::edgeParity()
extern void CubieCube_edgeParity_m8FC30006DE6D217519E9EEA544BDBD3CFF5986CB (void);
// 0x00000066 System.Int16 Kociemba.CubieCube::getFRtoBR()
extern void CubieCube_getFRtoBR_m2154C434ED069155E8849234170966B9C66FADF2 (void);
// 0x00000067 System.Void Kociemba.CubieCube::setFRtoBR(System.Int16)
extern void CubieCube_setFRtoBR_m0D39B3B8EFBF87232062D50C84B10270014CA0F4 (void);
// 0x00000068 System.Int16 Kociemba.CubieCube::getURFtoDLF()
extern void CubieCube_getURFtoDLF_m1686D00F1C9C5B48FAC263A60FF8BC48AD53219F (void);
// 0x00000069 System.Void Kociemba.CubieCube::setURFtoDLF(System.Int16)
extern void CubieCube_setURFtoDLF_m446700745A509853C168795A8B4CE1D395493C5F (void);
// 0x0000006A System.Int32 Kociemba.CubieCube::getURtoDF()
extern void CubieCube_getURtoDF_m434C9F2FA4444E3AE2C947ACFA96A46378EF7F6F (void);
// 0x0000006B System.Void Kociemba.CubieCube::setURtoDF(System.Int32)
extern void CubieCube_setURtoDF_m008303E214990B7F62588D3966FBF7C87701C0BA (void);
// 0x0000006C System.Int32 Kociemba.CubieCube::getURtoDF(System.Int16,System.Int16)
extern void CubieCube_getURtoDF_m9D4FFCFD06D0A9ADFE16378AF6B3C45B2251DF31 (void);
// 0x0000006D System.Int16 Kociemba.CubieCube::getURtoUL()
extern void CubieCube_getURtoUL_m7F820F1EA1B0258E77842FCFE1E383DEDCA181B6 (void);
// 0x0000006E System.Void Kociemba.CubieCube::setURtoUL(System.Int16)
extern void CubieCube_setURtoUL_m342E9E146DC6D058DD77C8A04FF88BE90E3D68D4 (void);
// 0x0000006F System.Int16 Kociemba.CubieCube::getUBtoDF()
extern void CubieCube_getUBtoDF_m8AE4B370437A7BEB2E05EA128229B47E0A7ED1FE (void);
// 0x00000070 System.Void Kociemba.CubieCube::setUBtoDF(System.Int16)
extern void CubieCube_setUBtoDF_m215C742A5B83C4FFB92190F53BB58F7C9AF850E4 (void);
// 0x00000071 System.Int32 Kociemba.CubieCube::getURFtoDLB()
extern void CubieCube_getURFtoDLB_m63B321B3A4C714B969110CCA53C5A96044D69C40 (void);
// 0x00000072 System.Void Kociemba.CubieCube::setURFtoDLB(System.Int32)
extern void CubieCube_setURFtoDLB_mEDA20E1C30F47C01A7C3F066FE47B400F6074103 (void);
// 0x00000073 System.Int32 Kociemba.CubieCube::getURtoBR()
extern void CubieCube_getURtoBR_mCB1730DCA96D9EF87238C80CFF07F230F6BD39B5 (void);
// 0x00000074 System.Void Kociemba.CubieCube::setURtoBR(System.Int32)
extern void CubieCube_setURtoBR_m54C3DF0950280962358BF31663A7CF0B9F273C7B (void);
// 0x00000075 System.Int32 Kociemba.CubieCube::verify()
extern void CubieCube_verify_m66760BD7C24731E8C9170A0E971EED79D95DD5EE (void);
// 0x00000076 System.Void Kociemba.CubieCube::.cctor()
extern void CubieCube__cctor_mD6F47016B720FA36863B1DB555BEE282484CBF61 (void);
// 0x00000077 System.Void Kociemba.FaceCube::.ctor()
extern void FaceCube__ctor_mBC5DB36499B9F83670FD377F1421FD7003172D6A (void);
// 0x00000078 System.Void Kociemba.FaceCube::.ctor(System.String)
extern void FaceCube__ctor_m0DFD35B7A5371313691F6AF9FA280163E1B3C51A (void);
// 0x00000079 System.String Kociemba.FaceCube::to_fc_String()
extern void FaceCube_to_fc_String_mBAF1B8199C6AAAC2B883A16DB7E6A783E848E9A0 (void);
// 0x0000007A Kociemba.CubieCube Kociemba.FaceCube::toCubieCube()
extern void FaceCube_toCubieCube_m8E1FCCB57A37F0BC0F2E00E39CEBDC30C9C1F8FB (void);
// 0x0000007B System.Void Kociemba.FaceCube::.cctor()
extern void FaceCube__cctor_mEB5F484A6F2D832C0E8CC26DED4FEEDB48C1CD7A (void);
// 0x0000007C System.String Kociemba.Search::solutionToString(System.Int32)
extern void Search_solutionToString_mABA5689EB64C5262A9C0EECB64AE020E5134DAE4 (void);
// 0x0000007D System.String Kociemba.Search::solutionToString(System.Int32,System.Int32)
extern void Search_solutionToString_m63CF0CE12562551144A23B94AA328C2EB40F7288 (void);
// 0x0000007E System.String Kociemba.Search::solution(System.String,System.String&,System.Int32,System.Int64,System.Boolean)
extern void Search_solution_m4BC5FA059863B43F9BE074EAA71606ED7CD41C41 (void);
// 0x0000007F System.Int32 Kociemba.Search::totalDepth(System.Int32,System.Int32)
extern void Search_totalDepth_mF3213568138D59ABBD38C3DBE905F7B5A27D8286 (void);
// 0x00000080 System.Void Kociemba.Search::.ctor()
extern void Search__ctor_m4E3BB53AD62C415E8BFA9A948403C79F816575A0 (void);
// 0x00000081 System.Void Kociemba.Search::.cctor()
extern void Search__cctor_mF9F65A692159CED3C2D62B68148D5BD5025F83C7 (void);
// 0x00000082 System.Int64 Kociemba.DateTimeHelper::CurrentUnixTimeMillis()
extern void DateTimeHelper_CurrentUnixTimeMillis_m93014DAF143866C0B33499D376D8BCFCD7D4CCAE (void);
// 0x00000083 System.Void Kociemba.DateTimeHelper::.cctor()
extern void DateTimeHelper__cctor_mDD7C9820F85858EDA7602EC8D92CA2A2B7FDFA8B (void);
// 0x00000084 System.String Kociemba.SearchRunTime::solutionToString(System.Int32)
extern void SearchRunTime_solutionToString_mE76A66599CC760BFB88EE9D281EBBBC1D5CE9884 (void);
// 0x00000085 System.String Kociemba.SearchRunTime::solutionToString(System.Int32,System.Int32)
extern void SearchRunTime_solutionToString_m4350A1AE576A1E2EE1F6874F6F4B16863CC3F7AE (void);
// 0x00000086 System.String Kociemba.SearchRunTime::solution(System.String,System.String&,System.Int32,System.Int64,System.Boolean,System.Boolean)
extern void SearchRunTime_solution_m26F1C1375F8D4E7D71226612C625591D376A3E50 (void);
// 0x00000087 System.Int32 Kociemba.SearchRunTime::totalDepth(System.Int32,System.Int32)
extern void SearchRunTime_totalDepth_m551B39C448DBAEF0442405A25A37901917B87A6E (void);
// 0x00000088 System.Void Kociemba.SearchRunTime::.ctor()
extern void SearchRunTime__ctor_mD5553018A3D9D97662EEEB7AFF3B6CE6B977129C (void);
// 0x00000089 System.Void Kociemba.SearchRunTime::.cctor()
extern void SearchRunTime__cctor_m82B1518987B74A1FEF86675C38E6D2843163C950 (void);
// 0x0000008A System.Int32 Kociemba.Tools::verify(System.String)
extern void Tools_verify_mA4D266B9B869B348EE9DA290C742AE98B1B95897 (void);
// 0x0000008B System.String Kociemba.Tools::randomCube()
extern void Tools_randomCube_m1D03C1AD125FD5A0095F721002F9782916D07283 (void);
// 0x0000008C System.Void Kociemba.Tools::SerializeTable(System.String,System.Int16[,])
extern void Tools_SerializeTable_m7207C51635C7889ADE6652F0F19F16EDAFE08660 (void);
// 0x0000008D System.Int16[,] Kociemba.Tools::DeserializeTable(System.String)
extern void Tools_DeserializeTable_mD855BCB3B71DD8FC71A777D581D6661BC9499C7A (void);
// 0x0000008E System.Void Kociemba.Tools::SerializeSbyteArray(System.String,System.SByte[])
extern void Tools_SerializeSbyteArray_m239D84651B065595BC5CBA3136EDBC9DFAAD6016 (void);
// 0x0000008F System.SByte[] Kociemba.Tools::DeserializeSbyteArray(System.String)
extern void Tools_DeserializeSbyteArray_mCB2C82B5B2095B1616EC7AC8BCD4C1151921923E (void);
// 0x00000090 System.Void Kociemba.Tools::EnsureFolder(System.String)
extern void Tools_EnsureFolder_mC7228C552F48C49AF2C6CF12AD4CF63B4DFB62F1 (void);
// 0x00000091 System.Void Kociemba.Tools::.ctor()
extern void Tools__ctor_m27AD90C0BAD0989E0A4E3DCBAAD55A2955AC88D5 (void);
static Il2CppMethodPointer s_methodPointers[145] = 
{
	Automate_Start_mBDB0AF6E210594E747FCF3F89B7603AD0E3CD830,
	Automate_DoMove_m4F15202D7B3A33C0188CF9BB02DE1B6B7F08D4F1,
	Automate_Update_m34C1E56FF6EF2E6650A5E44296CEA916C8009C87,
	Automate_RotateSide_m2A90885AE972850C24FA51C5DBFA63CC65008D99,
	Automate_Shuffle_m2FC45E731D05A8321449F78B3E04812B61F13221,
	Automate__ctor_m5D87D2488028FEB05629C5FA1C12DA1F4D00DF3A,
	Automate__cctor_mEB1D86BAC29210F348DB2C7DF6DF1FC189DCBBBF,
	CubeMap_Start_mC6C563E05776B0A0620CD814B3677471A400AB2B,
	CubeMap_Update_mE4F60140B1651C3C44DACF3AA5AB616BE4271B9C,
	CubeMap_Set_m615B714D1D5802CA5611CFF04326F78BDDBA8DF8,
	CubeMap_UpdateMap_mCC226C86B1F042627D8601B54047E3BC5053A107,
	CubeMap__ctor_m4817E9BA5EAB2AA13CB0813EA9165A52FBC363D5,
	CubeMap2x2_Start_mD254DE5DE9661A8BE9E594082FDB25D98651B30E,
	CubeMap2x2_Update_m8FFD84CA1CE28D1D397EF34BE481D16FA79785B6,
	CubeMap2x2_Set_m270BBB103090C4316BF67E7B1CBF9DF3689FCBD5,
	CubeMap2x2_UpdateMap_m089088BDDA6590467C40C1CB00A1D6ED6DA433A4,
	CubeMap2x2__ctor_mBDC2C176325512FADD5E8E8FD243F7B0A525E6CD,
	CubeState_Start_mEA2AA4C1915D4066A2832B6B09E0212D8FD2769F,
	CubeState_Update_m8576056AFD6EFAD1AC156B622F05FA5E7899992E,
	CubeState_PickUp_m87F4D7675578FD605B8734B954700BE931777E8D,
	CubeState_PutDown_mC4920A3F8E8E994A725C711194BC2837489F4E63,
	CubeState_GetSideString_mAA408019C3B3E72E5FC67F83F58D9F63BE3777F0,
	CubeState_GetStateString_m76ED7BF10189A348F41AF0ECF0C91EED5E845FAD,
	CubeState__ctor_m9AC614D3D99268EB42439D4B44DF0F6993571A66,
	PivotRotation_Start_m86D01248253987BDC15EB167889A95FFABA80B94,
	PivotRotation_LateUpdate_m711A34DA3E5604B83DCE1BB5285CB98BA86BA2E0,
	PivotRotation_SpinSide_m92B1DE185796F59EF61BB269B8528F89CC114EBA,
	PivotRotation_Rotate_mC187B184E0DD7BE4C151331B1CE3361AF66AFE1B,
	PivotRotation_StartAutoRotate_m2ADC5074BCF8D24083EF59E70686735F73371B2C,
	PivotRotation_RotateToRightAngle_mD451F4278A3EA0FD69A7DAF519D281405C12769A,
	PivotRotation_AutoRotate_m02783AE1AD1051CE09DD092DC59770B505340F34,
	PivotRotation__ctor_mA5504347F2AD2C268AD02F51F990B37D48136B79,
	ReadCube_Start_mCBB1787A677FEFF0F13F849733DEA72C8B1220D9,
	ReadCube_Update_m22D3232C518D5908C9CBFC02DDD1D6FEEA829016,
	ReadCube_ReadState_mE1CE672EE789CD1F061A456D5EFEAA86D7214C4F,
	ReadCube_SetRayTransforms_m1BF7F2B9C2F28422AC87CCD9F88F3E8DC8005F33,
	ReadCube_BuildRays_m66BCF4EC0D81F94ED9371288422754E676E4D8EB,
	ReadCube_ReadFace_m62F0200A60B46E0DE485C988CEEB42EFB0BAAB44,
	ReadCube__ctor_m86B33B35EFB6179CF6D880471F5023F2516276BC,
	ReadCube2x2_Start_m4629275DE89CDD940D92C2FFB0C882C73AAC9AFE,
	ReadCube2x2_Update_m30509ED389B2277C62C8FCF192D00A42F4FCD332,
	ReadCube2x2__ctor_mCF0F803238D00B8F922866347F5DA783EC5490AD,
	RotateBigCube_Start_m11CF76FABEB44B5D94E9428424121B05F27BFA4E,
	RotateBigCube_Update_m602101730D3E7BF12AD8861B98A454D642F6FFE9,
	RotateBigCube_Drag_m6638ADF4EF83C704461ECBC8F4F090879890039B,
	RotateBigCube_Swipe_m23796E9DCA6E660401B5D1A08639CEF10D01E28B,
	RotateBigCube_LeftSwipe_m8BEC0F12B23361056169481F41310842C8AD8CF0,
	RotateBigCube_RightSwipe_m27273211A07E0260B1AC83D37717860E17794826,
	RotateBigCube_UpLeftSwipe_mA900D93941403FDE22CC9DF050D30A0479E56CBC,
	RotateBigCube_UpRightSwipe_mD76FE6B96A6CD970C073CFEBCBBABE68CA164FE4,
	RotateBigCube_DownLeftSwipe_m7DF37C53EFFBD47FCEC6F724D8012B73E24E49E7,
	RotateBigCube_DownRightSwipe_m7FD9A200C38131A702F6678E47396E0BE44227C2,
	RotateBigCube__ctor_m775AC20AC064C99B8ECD953DBF231F90889BAAE9,
	SelectFace_Start_m6B8CAF850E5A0DE138D58875FE17FD796682A0AB,
	SelectFace_Update_mEA3B8E67A4A36842BB1D70343AE8B10830C78615,
	SelectFace__ctor_mE2D218F6E91D0DDBBD0CE9062D15EE5DC4EB7FA6,
	SolveTwoPhase_Start_m0BDA3CE525AACFEC1DDEE70D8BFFC30C68BF7595,
	SolveTwoPhase_Update_mF7D483EE9639F9DB8BF78A0E4AF9F66283B29F1A,
	SolveTwoPhase_Solver_m6ECBAC39E2EE97BE34027BBC08992FD5EB98026C,
	SolveTwoPhase_StringToList_m43419C5E86322DEDB05A0689E74FCE995F78CF64,
	SolveTwoPhase__ctor_mC6865BD683042E5DDB12492BF29FC1C70D517789,
	UIConfig_Start_m25B7619856DEB3AEE96DA33C6E3F0848BFE95EF8,
	UIConfig_Update_m3B8D2CF963D09ECD9E9F04F3F85D6FDBD7CFA07B,
	UIConfig_OpenPanel_m018C4AA7A9556A24926B64CC53E75A25FCA5B026,
	UIConfig_ClosePanel_mAEB1D424F5149A6B34B3EE12112E8C04FE305FEC,
	UIConfig__ctor_mAE057D107A8E87DBC0CEEEB5E294E97C61B254D0,
	CoordCube__ctor_mFA5B2A4172D7942C1BAEE609FFBD9FABD936D2E8,
	CoordCube_move_m8D8DBD684BB5D54613A361246B5EF598DA90B33E,
	CoordCube_setPruning_mE9A6455AF77E31D9223B2D7ABE557521C45D01F9,
	CoordCube_getPruning_mE8CAF02D3C74122D1804E2883BCB3CA5B6D27894,
	CoordCube__cctor_mFD2EC6E61CD35995CAEE73EFCD26B3DDC38B5E9E,
	CoordCubeTables__cctor_m85AACC9F1F41BA4705EF57305973314B07CED64F,
	CoordCubeBuildTables__ctor_m8AB3DA09766C3FCA0B88ECB408FD2821B8652E4D,
	CoordCubeBuildTables_move_mAC7124234589FB1979CE9E2806602C0FBD80984C,
	CoordCubeBuildTables__cctor_mF2E1E6437BB68FF0171C0D501D966014132E3BC6,
	CoordCubeBuildTables_setPruning_m6FF26F7B216589625A69C06154F7AA71F6B83AEC,
	CoordCubeBuildTables_getPruning_m642B13917BD8B3BD134D276959DA94B5A9DB8CF9,
	CubieCube_SetMoveU_m7D4FAE08297C79C16FB107639CE14F649AC50174,
	CubieCube_SetMoveR_m6A6140987EEA4B70D770615D59787E2D1499518C,
	CubieCube_SetMoveF_mF3C7898A58C45515845574F73B5B63286D54CB33,
	CubieCube_SetMoveD_m32C25AF0AB36F4B8604E13C310423B7663B41337,
	CubieCube_SetMoveL_m3180D3A6CE06A9A515D63C28200107BAA4B70DCB,
	CubieCube_SetMoveB_m1F735EFBF6457E2095CC087ADD4957F9D04A20DE,
	CubieCube__ctor_mB2558B39546E22E61E617B78DCB3C1C0C04C21F8,
	CubieCube__ctor_mAA57E665AB7511499DF2699079B62DC1E42E3C06,
	CubieCube_Cnk_m98D7B2B8A75B168B81B3242D387EB8F94A95678D,
	CubieCube_rotateLeft_m94DAB9B32516F2E4779EEF3BCC9B31155CA12E8C,
	CubieCube_rotateRight_mCCF1F161DB9D871DCD9E90E9DA676FACCB5A3302,
	CubieCube_rotateLeft_m4FC7EEA9E12AD2781CF4DA24851B80E88F2BFFFB,
	CubieCube_rotateRight_mB0355658688C284A59D6C17A1923BFA93E4334E7,
	CubieCube_toFaceCube_m69B0E445C8A6BA456898D7C81B0C994AFD8D424C,
	CubieCube_cornerMultiply_m5331F948D9A0B65AFFE8D2B98301035DB5107191,
	CubieCube_edgeMultiply_m0DD6CDD04C35145DA1EE9150FF692BF05015B508,
	CubieCube_multiply_m16F3D906044236B346C24058D98E21A060F35A69,
	CubieCube_invCubieCube_m9039266F9DF891EC002A9F14154547B2060AE3D8,
	CubieCube_getTwist_m7A91EEF586A0B3A87902730155F04B602B47B479,
	CubieCube_setTwist_m46C19E28D3BACE0B29EEC5943DE41EDF5A74FB2F,
	CubieCube_getFlip_m680327B28CFB582596E53C606CEA27761289C3BA,
	CubieCube_setFlip_m5BF099E8040B3EFD04F0A48E7FF23F1AF5F54D86,
	CubieCube_cornerParity_mB0E00AE6ED334FB985343E94D45AEF0F21E4DB90,
	CubieCube_edgeParity_m8FC30006DE6D217519E9EEA544BDBD3CFF5986CB,
	CubieCube_getFRtoBR_m2154C434ED069155E8849234170966B9C66FADF2,
	CubieCube_setFRtoBR_m0D39B3B8EFBF87232062D50C84B10270014CA0F4,
	CubieCube_getURFtoDLF_m1686D00F1C9C5B48FAC263A60FF8BC48AD53219F,
	CubieCube_setURFtoDLF_m446700745A509853C168795A8B4CE1D395493C5F,
	CubieCube_getURtoDF_m434C9F2FA4444E3AE2C947ACFA96A46378EF7F6F,
	CubieCube_setURtoDF_m008303E214990B7F62588D3966FBF7C87701C0BA,
	CubieCube_getURtoDF_m9D4FFCFD06D0A9ADFE16378AF6B3C45B2251DF31,
	CubieCube_getURtoUL_m7F820F1EA1B0258E77842FCFE1E383DEDCA181B6,
	CubieCube_setURtoUL_m342E9E146DC6D058DD77C8A04FF88BE90E3D68D4,
	CubieCube_getUBtoDF_m8AE4B370437A7BEB2E05EA128229B47E0A7ED1FE,
	CubieCube_setUBtoDF_m215C742A5B83C4FFB92190F53BB58F7C9AF850E4,
	CubieCube_getURFtoDLB_m63B321B3A4C714B969110CCA53C5A96044D69C40,
	CubieCube_setURFtoDLB_mEDA20E1C30F47C01A7C3F066FE47B400F6074103,
	CubieCube_getURtoBR_mCB1730DCA96D9EF87238C80CFF07F230F6BD39B5,
	CubieCube_setURtoBR_m54C3DF0950280962358BF31663A7CF0B9F273C7B,
	CubieCube_verify_m66760BD7C24731E8C9170A0E971EED79D95DD5EE,
	CubieCube__cctor_mD6F47016B720FA36863B1DB555BEE282484CBF61,
	FaceCube__ctor_mBC5DB36499B9F83670FD377F1421FD7003172D6A,
	FaceCube__ctor_m0DFD35B7A5371313691F6AF9FA280163E1B3C51A,
	FaceCube_to_fc_String_mBAF1B8199C6AAAC2B883A16DB7E6A783E848E9A0,
	FaceCube_toCubieCube_m8E1FCCB57A37F0BC0F2E00E39CEBDC30C9C1F8FB,
	FaceCube__cctor_mEB5F484A6F2D832C0E8CC26DED4FEEDB48C1CD7A,
	Search_solutionToString_mABA5689EB64C5262A9C0EECB64AE020E5134DAE4,
	Search_solutionToString_m63CF0CE12562551144A23B94AA328C2EB40F7288,
	Search_solution_m4BC5FA059863B43F9BE074EAA71606ED7CD41C41,
	Search_totalDepth_mF3213568138D59ABBD38C3DBE905F7B5A27D8286,
	Search__ctor_m4E3BB53AD62C415E8BFA9A948403C79F816575A0,
	Search__cctor_mF9F65A692159CED3C2D62B68148D5BD5025F83C7,
	DateTimeHelper_CurrentUnixTimeMillis_m93014DAF143866C0B33499D376D8BCFCD7D4CCAE,
	DateTimeHelper__cctor_mDD7C9820F85858EDA7602EC8D92CA2A2B7FDFA8B,
	SearchRunTime_solutionToString_mE76A66599CC760BFB88EE9D281EBBBC1D5CE9884,
	SearchRunTime_solutionToString_m4350A1AE576A1E2EE1F6874F6F4B16863CC3F7AE,
	SearchRunTime_solution_m26F1C1375F8D4E7D71226612C625591D376A3E50,
	SearchRunTime_totalDepth_m551B39C448DBAEF0442405A25A37901917B87A6E,
	SearchRunTime__ctor_mD5553018A3D9D97662EEEB7AFF3B6CE6B977129C,
	SearchRunTime__cctor_m82B1518987B74A1FEF86675C38E6D2843163C950,
	Tools_verify_mA4D266B9B869B348EE9DA290C742AE98B1B95897,
	Tools_randomCube_m1D03C1AD125FD5A0095F721002F9782916D07283,
	Tools_SerializeTable_m7207C51635C7889ADE6652F0F19F16EDAFE08660,
	Tools_DeserializeTable_mD855BCB3B71DD8FC71A777D581D6661BC9499C7A,
	Tools_SerializeSbyteArray_m239D84651B065595BC5CBA3136EDBC9DFAAD6016,
	Tools_DeserializeSbyteArray_mCB2C82B5B2095B1616EC7AC8BCD4C1151921923E,
	Tools_EnsureFolder_mC7228C552F48C49AF2C6CF12AD4CF63B4DFB62F1,
	Tools__ctor_m27AD90C0BAD0989E0A4E3DCBAAD55A2955AC88D5,
};
static const int32_t s_InvokerIndices[145] = 
{
	3421,
	2787,
	3421,
	1545,
	3421,
	3421,
	5152,
	3421,
	3421,
	3421,
	1542,
	3421,
	3421,
	3421,
	3421,
	1542,
	3421,
	3421,
	3421,
	2787,
	1542,
	2454,
	3329,
	3421,
	3421,
	3421,
	2787,
	2787,
	1545,
	3421,
	3421,
	3421,
	3421,
	3421,
	3421,
	3421,
	1193,
	1192,
	3421,
	3421,
	3421,
	3421,
	3421,
	3421,
	3421,
	3421,
	2052,
	2052,
	2052,
	2052,
	2052,
	2052,
	3421,
	3421,
	3421,
	3421,
	3421,
	3421,
	3421,
	2454,
	3421,
	3421,
	3421,
	3421,
	3421,
	3421,
	532,
	2769,
	4296,
	4581,
	5152,
	5152,
	1532,
	2769,
	5152,
	4296,
	4581,
	5126,
	5126,
	5126,
	5126,
	5126,
	5126,
	3421,
	580,
	4492,
	4294,
	4294,
	4294,
	4294,
	3329,
	2787,
	2787,
	2787,
	2787,
	3308,
	2768,
	3308,
	2768,
	3308,
	3308,
	3308,
	2768,
	3308,
	2768,
	3309,
	2769,
	4490,
	3308,
	2768,
	3308,
	2768,
	3309,
	2769,
	3309,
	2769,
	3309,
	5152,
	3421,
	2787,
	3329,
	3329,
	5152,
	4911,
	4532,
	3724,
	4492,
	3421,
	5152,
	5120,
	5152,
	4911,
	4532,
	3580,
	4492,
	3421,
	5152,
	4858,
	5126,
	4683,
	4914,
	4683,
	4914,
	5052,
	3421,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	145,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
