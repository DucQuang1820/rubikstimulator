using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kociemba;
using UnityEngine.UI;

public class SolveTwoPhase : MonoBehaviour
{
    public ReadCube readCube;
    public CubeState cubeState;
    private bool doOnce = true;
    public Button btn_Shuffle;

    // Start is called before the first frame update
    void Start()
    {
        readCube = FindObjectOfType<ReadCube>();
        cubeState = FindObjectOfType<CubeState>();
    }

    // Update is called once per frame
    void Update()
    {
        if(CubeState.started && doOnce)
        {
            doOnce = false;
            Solver();
        }
    }

    public void Solver()
    {
        btn_Shuffle.interactable = false;
        if(!CubeState.autoRotating)
        {
            readCube.ReadState(); 

            string moveString = cubeState.GetStateString();
            print(moveString);

            string info = "";

            //string solution = SearchRunTime.solution(moveString, out info, buildTables: true);

            string solution = Search.solution(moveString, out info);

            List<string> solutionList = StringToList(solution);
            
            Automate.moveList = solutionList;

            print(info);
        }
        
    }

    List<string> StringToList(string solution)
    {
        List<string> solutionList = new List<string>(solution.Split(new string [] { " " }, System.StringSplitOptions.RemoveEmptyEntries));
        return solutionList;
    }
}
