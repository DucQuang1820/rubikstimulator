using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIConfig : MonoBehaviour
{
    public GameObject HelpPanel;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OpenPanel()
    {
        HelpPanel.SetActive(true);
    }

    public void ClosePanel()
    {
        HelpPanel.SetActive(false);
    }

}
